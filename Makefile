all: txt html

html:
	xml2rfc --html --css style.css draft-grothoff-taler.xml

txt:
	xml2rfc draft-grothoff-taler.xml

